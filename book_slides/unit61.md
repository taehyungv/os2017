---

# Advanced File permissions

SUID

   - For file: Program runs with effective user ID of owner 
   - For directory: No effect

SGID

   - For file: Program runs with effective group ID of owner 
   - For directory: Files created in directory inherit the same group ID as the directory

Sticky bit

   - For file: No effect
   - For directory: Only the owner of the file and the owner of the directory may delete files in this directory 

---

# Changing permissions

- Setting file permissions is done with the **chmod** command

    \# chmod 1755 (or o+t) commondir  
    \# chmod 2755 (or g+s) myprog  
    \# chmod 4755 (or u+s) passwd  

